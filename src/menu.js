export default class Menu {
  constructor(){
    document.getElementById('main-menu-close').addEventListener('click', this.closeMenu);
    document.getElementById('main-menu-button').addEventListener('click', this.openMenu);
    const menuItems = document.querySelectorAll('#main-menu > ul > li');
    menuItems.forEach(item => item.firstElementChild.addEventListener('click', () => this.closeMenu()));
  }
  
  openMenu = (event) => {
    event.preventDefault();
    document.getElementById('main-menu').classList.add('opened');
  }
  
  closeMenu = (event) => {
    if(event) event.preventDefault();
    document.getElementById('main-menu').classList.remove('opened');
  }
}
