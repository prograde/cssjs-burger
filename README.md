# cssjs-burger

(I wish I could say it should be pronounced 'cheese-burger', but you wouldn't agree ...)

A simple web site template with a JS driven hamburger menu that will fallback to CSS if the user has disabled JS.

Inspired by https://medium.com/@heyoka/responsive-pure-css-off-canvas-hamburger-menu-aebc8d11d793